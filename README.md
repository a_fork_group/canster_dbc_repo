Please adhere to the standard element naming:
1. All heartbeats: 100 to 199
2. Other Nodes' MSG IDs: 200, 300, 400 etc.

--

Steps to commit your updated DBC file to this repo:
1. git remote add dbc_origin https://gitlab.com/a_fork_group/canster_dbc_repo
2. git add <your_project.dbc>
3. git commit -m "Your updated message with MSG ID and functionality"
4. git push dbc_origin master (Enter GitLab credentials if prompted)